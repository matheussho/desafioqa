package steps;

import org.openqa.selenium.WebElement;

import page.InicialPage;

public class InicialSteps {

	InicialPage inicialPage = new InicialPage();

	public WebElement title = inicialPage.title;


	/**
	 * Metodo que acessa o menu guia medico
	 * 
	 * @param email
	 */
	public void acessarMenuGuiaMedico() {
		inicialPage.clicarBotaoMenuGuiaMedico();

	}

}
