package steps;

import org.openqa.selenium.WebElement;

import page.RedeAssistencialPage;

public class RedeAssistencialSteps {

	RedeAssistencialPage redeAssistencialPage = new RedeAssistencialPage();

	public WebElement txtEndereco = redeAssistencialPage.txtEndereco;
	public WebElement txtServicos = redeAssistencialPage.txtServicos;

	/**
	 * Metodo que pesquisa na rede de assistencia.
	 * 
	 * @param valor
	 */
	public void pesquisarPorMedico(String valor, String estado, String cidade, boolean paginacao, int numeroPagina) {
		if (!valor.isEmpty()) {
			redeAssistencialPage.preencherCampoPesquisa(valor);
		}

		redeAssistencialPage.clicarBotaoPesquisar();

		redeAssistencialPage.preencherEstado(estado);
		redeAssistencialPage.preencherCidade(cidade);

		redeAssistencialPage.clicarRadioButtonRegiaoPreferencia();
		redeAssistencialPage.clicarBotaoContinuar();
		redeAssistencialPage.selecionarOrdenacaoConsulta();

		verificarPaginacao(paginacao, numeroPagina);

	}

	public boolean verificarPaginacao(boolean paginacao, int numeroPagina) {
		if (paginacao) {
			if (numeroPagina == 2) {
				redeAssistencialPage.clicarPaginaDois();
			} else if (numeroPagina == 3) {
				redeAssistencialPage.clicarPaginaTres();
			}
		}

		return paginacao;
	}

}
