package base;

import org.junit.Before;

import helpers.DriverFactory;
import helpers.Navegador;
import utils.ConfigUtil;

/**
 * Classe base para todos os testes
 * 
 * @author Matheus
 *
 */
public class BaseTest {

	public BaseTest() {
		if (DriverFactory.driver == null) {
			DriverFactory.iniciarNavegador(ConfigUtil.lerArquivoProperties("chrome"));
		}
	}

	@Before
	public void setUp() {
		DriverFactory.driver.manage().deleteAllCookies();
		abrirPagina();
	}

//	@After
//	public void tearDown() {
//		DriverFactory.driver.quit();
//	}

	public void abrirPagina() {
		Navegador.navegarParaUrl(DriverFactory.baseUrl);
	}
}
