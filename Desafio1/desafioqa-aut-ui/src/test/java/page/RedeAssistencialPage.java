package page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import helpers.DriverFactory;
import utils.InteracoesSelenium;

/**
 * Classe que contem o mapeamento da pesquisa da rede assistencial
 * 
 * @author Matheus
 *
 */
public class RedeAssistencialPage {
	public RedeAssistencialPage() {
		PageFactory.initElements(DriverFactory.driver, this);
	}

	@FindBy(how = How.ID, using = "campo_pesquisa")
	public WebElement txtCampoPesquisa;

	@FindBy(how = How.ID, using = "btn_pesquisar")
	public WebElement btnPesquisar;

	@FindBy(how = How.XPATH, using = "//div/h1[@class='titulo-midia cor-clara']")
	public WebElement title;

	@FindBy(how = How.ID, using = "react-select-3-input")
	public WebElement txtCidade;

	@FindBy(how = How.ID, using = "react-select-2-input")
	public WebElement txtEstado;

	@FindBy(how = How.XPATH, using = "//div/button[@class='btn btn-success']")
	public WebElement btnContinuar;

	@FindBy(how = How.XPATH, using = "//div/input[@type='radio']")
	public WebElement rdRegiaoPreferencia;
	
	@FindBy(how = How.XPATH, using = "//form[@class='form-escolher-unimed-gm']/label/div[@class='margin-left']")
	public WebElement txtRegiaoPreferencia;
	
	@FindBy(how = How.ID, using = "select_ordenar_prestador")
	public WebElement slcOrdenarPrestador;

	@FindBy(how = How.XPATH, using = "//div[@id='resultado0']/div/span[@id='txt_endereco']/p")
	public WebElement txtEndereco;

	@FindBy(how = How.XPATH, using = "//div[@id='resultado0']/div/p/span[2]")
	public WebElement txtServicos;

	@FindBy(how = How.LINK_TEXT, using = "2")
	public WebElement lnkPage2;

	@FindBy(how = How.LINK_TEXT, using = "3")
	public WebElement lnkPage3;

	public void preencherCampoPesquisa(String valor) {
		InteracoesSelenium.escreverNoInput(txtCampoPesquisa, valor);
	}

	public void clicarBotaoPesquisar() {
		InteracoesSelenium.clicarBotao(btnPesquisar, btnPesquisar.getText());
	}

	public void preencherEstado(String estado) {
		InteracoesSelenium.escreverComboBox(txtEstado, estado);

	}

	public void preencherCidade(String cidade) {
		InteracoesSelenium.escreverComboBox(txtCidade, cidade);
	}

	public void clicarBotaoContinuar() {
		InteracoesSelenium.clicarBotao(btnContinuar, btnContinuar.getText());
	}

	public void clicarRadioButtonRegiaoPreferencia() {
		InteracoesSelenium.clicarRadioButton(txtRegiaoPreferencia, txtRegiaoPreferencia.getText());
	}

	public void selecionarOrdenacaoConsulta() {
		InteracoesSelenium.selectElementoByValue(slcOrdenarPrestador, "SCORE");
	}

	public void clicarPaginaDois() {
		InteracoesSelenium.clicarBotao(lnkPage2, lnkPage2.getText());
	}

	public void clicarPaginaTres() {
		InteracoesSelenium.clicarBotao(lnkPage3, lnkPage3.getText());
	}

}
