package page;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import helpers.DriverFactory;
import utils.InteracoesSelenium;

/**
 * Class que contem o mapeamento da pagina inicial
 * 
 * @author Matheus
 *
 */
public class InicialPage {

	public InicialPage() {
		PageFactory.initElements(DriverFactory.driver, this);
	}

	@FindBy(how = How.XPATH, using = "//li/a[@href='https://www.unimed.coop.br/guia-medico']")
	public WebElement btnGuiaMedico;

	@FindBy(how = How.XPATH, using = "//div/h1[@class='titulo-midia cor-clara']")
	public WebElement title;

	public void clicarBotaoMenuGuiaMedico() {
		InteracoesSelenium.clicarBotao(btnGuiaMedico, btnGuiaMedico.getText());
	}

}
