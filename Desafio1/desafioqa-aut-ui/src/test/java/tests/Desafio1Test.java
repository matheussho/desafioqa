package tests;

import static org.junit.Assert.assertFalse;

import org.junit.Test;

import base.BaseTest;
import helpers.DriverFactory;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Title;
import steps.InicialSteps;
import steps.RedeAssistencialSteps;
import utils.ConfigUtil;
import utils.ValidacaoAssert;

@Title("DESAFIO 1")
@Features("FT01 - Realizar uma pesquisa de médicos no Rio de Janeiro e validar apresentação do resultados")
public class Desafio1Test extends BaseTest {

	InicialSteps inicialSteps = new InicialSteps();
	RedeAssistencialSteps rede = new RedeAssistencialSteps();

	@Test
	@Title("[TESTE - 001] - Verificar pesquisa realizada e validar a cidade pesquisada.")
	public void testValidarPesquisaCidadeEspecialista() {
		inicialSteps.acessarMenuGuiaMedico();
		rede.pesquisarPorMedico(ConfigUtil.lerArquivoProperties("pesquisa"), ConfigUtil.lerArquivoProperties("estado"),
				ConfigUtil.lerArquivoProperties("cidade"), false, 0);
		ValidacaoAssert.assertMessageInElement(rede.txtEndereco, "Rio de Janeiro");
	}

	@Test
	@Title("[TESTE - 002] - Verificar pesquisa realizada e validar o tipo de servico ou especialidade.")
	public void testValidarPesquisaTipoServico() {
		inicialSteps.acessarMenuGuiaMedico();
		rede.pesquisarPorMedico(ConfigUtil.lerArquivoProperties("pesquisa"), ConfigUtil.lerArquivoProperties("estado"),
				ConfigUtil.lerArquivoProperties("cidade"), false, 0);

		ValidacaoAssert.assertMessageInElement(rede.txtServicos, "Cirurgia Oftalmológica");
	}

	@Test
	@Title("[TESTE - 003] - Verificar pesquisa realizada e validar na paginas 1 que nao contem os resultados com cidade de sao paulo.")
	public void testValidarPesquisaSemCidadeSaoPauloPaginaUm() {
		inicialSteps.acessarMenuGuiaMedico();
		rede.pesquisarPorMedico("RIO DE JANEIRO", "Rio de Janeiro", "Rio de Janeiro", false, 0);
		assertFalse(DriverFactory.driver.getPageSource().contains("Sao Paulo"));
	}

	@Test
	@Title("[TESTE - 004] - Verificar pesquisa realizada e validar nas paginas 2 que nao contem os resultados com cidade de sao paulo.")
	public void testValidarPesquisaSemCidadeSaoPauloPaginaDois() {
		inicialSteps.acessarMenuGuiaMedico();
		rede.pesquisarPorMedico(ConfigUtil.lerArquivoProperties("pesquisa"), ConfigUtil.lerArquivoProperties("estado"),
				ConfigUtil.lerArquivoProperties("cidade"), true, 2);
		assertFalse(DriverFactory.driver.getPageSource().contains("Sao Paulo"));

	}

	@Test
	@Title("[TESTE - 005] - Verificar pesquisa realizada e validar nas paginas 3 que nao contem os resultados com cidade de sao paulo.")
	public void testValidarPesquisaSemCidadeSaoPauloPaginaTres() {
		inicialSteps.acessarMenuGuiaMedico();

		rede.pesquisarPorMedico(ConfigUtil.lerArquivoProperties("pesquisa"), ConfigUtil.lerArquivoProperties("estado"),
				ConfigUtil.lerArquivoProperties("cidade"), true, 3);
		assertFalse(DriverFactory.driver.getPageSource().contains("Sao Paulo"));
	}

}