package helpers;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import utils.ConfigUtil;

/**
 * 
 * Classe que contem o DriverFactory e a inicializacao do browser.
 * 
 * @author Matheus
 *
 */
public class DriverFactory {
	public static WebDriver driver;

	public DriverFactory() {
		driver = null;
	}

	public static String getBaseUrl() {
		return baseUrl;
	}

	public static void setBaseUrl(String baseUrl) {
		DriverFactory.baseUrl = baseUrl;
	}

	public static String baseUrl;

	public static void iniciarNavegador(String browser) {
		if ("chrome".equalsIgnoreCase(browser)) {
			driver = getChromeDriver();
		} else if ("firefox".equalsIgnoreCase(browser)) {
			System.setProperty("webdriver.gecko.driver", ConfigUtil.lerArquivoProperties("firefox.driver"));
			driver = new FirefoxDriver();
		}
		baseUrl = ConfigUtil.lerArquivoProperties("url");

		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	private static WebDriver getChromeDriver() {
		WebDriver driver = new ChromeDriver(getChromeOptions());
		return driver;
	}

	private static ChromeOptions getChromeOptions() {
		ChromeOptions option = new ChromeOptions();
		option.addArguments("--start-maximized");
		return option;
	}

}
