package helpers;

import ru.yandex.qatools.allure.annotations.Step;

public class Navegador {

	@Step("Ir para a pagina => {0}")
	public static void navegarParaUrl(String url) {
		DriverFactory.driver.navigate().to(url);
	}

}
