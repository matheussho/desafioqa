package utils;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * Classe de configuracao para leitura do arquivo config.properties
 * 
 * @author Matheus
 *
 */
public class ConfigUtil {
	private ConfigUtil() {
	}

	/**
	 * Ler a propriedade do arquivo config.properties
	 * 
	 * @param property
	 * @return
	 */
	public static String lerArquivoProperties(String properties) {
		Properties prop;
		String valor = null;
		try {
			prop = new Properties();
			prop.load(new FileInputStream(new File("config.properties")));

			valor = prop.getProperty(properties);

			if (valor == null || valor.isEmpty()) {
				throw new Exception("Valor nāo definido ou vazio");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return valor;
	}
}
