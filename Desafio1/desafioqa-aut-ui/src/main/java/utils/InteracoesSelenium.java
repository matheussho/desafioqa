package utils;

import java.util.NoSuchElementException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import helpers.DriverFactory;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Classe que contem todas as interacoes com os componentes.
 * 
 * Ex. Inputs, Comboboxes, Botoes
 * 
 * @author Matheus
 *
 */
public class InteracoesSelenium {
	@Step("Clica no botao => {1}")
	public static void clicarBotao(WebElement elemento, String texto) {
		try {
			if (EsperasExplicitas.isClickableElement(elemento)) {
				elemento.click();
			}
		} catch (NoSuchElementException ex) {
			ex.getMessage();
		}

	}

	@Step("Clica no botao => {1}")
	public static void clicarRadioButton(WebElement elemento, String texto) {
		new WebDriverWait(DriverFactory.driver, 35).until(ExpectedConditions.textToBePresentInElement(elemento, texto));
		elemento.click();
	}

	@Step("Preencher Campo => {1}")
	public static void escreverNoInput(WebElement elemento, String texto) {
		new WebDriverWait(DriverFactory.driver, 35).until(ExpectedConditions.visibilityOf(elemento));
		if (texto != "") {
			elemento.clear();
			elemento.sendKeys(texto);
		}
	}

	@Step("Preencher Campo => {1}")
	public static void escreverComboBox(WebElement elemento, String texto) {
		new WebDriverWait(DriverFactory.driver, 35).until(ExpectedConditions.visibilityOf(elemento));
		if (texto != "") {
			elemento.clear();
			elemento.sendKeys(texto);
			elemento.sendKeys(Keys.TAB);
		}
	}

	@Step("Selecionar o campo Campo => {1}")
	public static void selectElementoByValue(WebElement elemento, String texto) {
		Select select = new Select(elemento);
		select.selectByValue(texto);
	}

}
