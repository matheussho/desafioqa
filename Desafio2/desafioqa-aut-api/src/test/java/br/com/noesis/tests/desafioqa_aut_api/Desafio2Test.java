package br.com.noesis.tests.desafioqa_aut_api;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

import io.restassured.http.ContentType;
import org.junit.Test;

public class Desafio2Test {

	public static final String IDIMDB = "tt1285016";
	public static final String KEY = "52ec71bf";
	public static String baseURI = "http://www.omdbapi.com/?i=" + IDIMDB + "&apikey=" + KEY;
	public static String uriInvalida = "http://www.omdbapi.com/?t=filmenaoexiste&y=2002";

	@Test
	public void testValidarTituloAnoEIdioma() {
		given().content(ContentType.JSON).when().get(baseURI).then().statusCode(200)
				.body("Title", equalTo("The Social Network")).body("Year", equalTo("2010"))
				.body("Language", equalTo("English, French"));
	}

	@Test
	public void testValidarBuscaInexistenteFilme() {
		given().content(ContentType.JSON).when().get(uriInvalida).then().statusCode(401).body("Error",
				equalTo("No API key provided."));
	}

}
