package br.com.noesis.tests.desafioqa_aut_tdd;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import atributos.Produto;
import service.CarrinhoService;

public class Desafio3Test {

	private CarrinhoService carrinho;

	@Before
	public void criandoCarrinho() {
		carrinho = new CarrinhoService();
	}

	private Produto id1 = new Produto(1, "Senhor dos Aneis", "Fantasia", 45.00);
	private Produto id2 = new Produto(2, "As Branquelas", "Comedia", 55.00);
	private Produto id3 = new Produto(3, "Velozes e Furiosos 7", "Acao", 100.00);
	private Produto id4 = new Produto(4, "Velozes e Furiosos 6", "Acao", 55.00);
	private Produto id5 = new Produto(5, "The Scapegoat", "Drama", 100.00);
	private Produto id6 = new Produto(6, "Meu Malvado Favorito", "Animacao", 200.00);

	@Test
	public void testObterDezPorcentoDeDesconto() {
		carrinho.add(id1);
		carrinho.add(id1);
		double precoFinal = carrinho.obterValorTotal();
		Assert.assertEquals(90.00, precoFinal, 0);
	}

	@Test
	public void testObterVintePorcentoDeDesconto() {
		carrinho.add(id3);
		carrinho.add(id6);
		double precoFinal = carrinho.obterValorTotal();
		assertEquals(240.00, precoFinal, 0);
	}

	@Test
	public void testObterVinteCincoPorcentoDeDesconto() {
		carrinho.add(id3);
		carrinho.add(id6);
		carrinho.add(id4);
		double precoFinal = carrinho.obterValorTotal();
		assertEquals(266.25, precoFinal, 0);
	}

	@Test
	public void testObterTrintaPorcentoDeDesconto() {
		carrinho.add(id3);
		carrinho.add(id6);
		carrinho.add(id2);
		carrinho.add(id5);
		double precoFinal = carrinho.obterValorTotal();
		assertEquals(318.50, precoFinal, 0);
	}

}