package atributos;

public class Produto {
	private int id;
	private String genero;
	private String filme;
	private double preco;

	public Produto(int id, String filme, String genero, double preco) {
		this.id = id;
		this.filme = filme;
		this.genero = genero;
		this.preco = preco;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getFilme() {
		return filme;
	}

	public void setFilme(String filme) {
		this.filme = filme;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

}
