package service;

import java.util.ArrayList;

import atributos.Produto;

public class CarrinhoService {
	private ArrayList<Produto> produtos = new ArrayList<Produto>();

	public CarrinhoService() {
		produtos = new ArrayList<>();
	}

	public void add(Produto produto) {
		produtos.add(produto);
	}

	public double obterValorTotal() {
		double total = 0;
		double desconto = 0;
		for (Produto produto : produtos) {
			total += produto.getPreco();
		}

		if (total >= 100 && total <= 200) {
			desconto = (total * 10) / 100;
		} else if (total > 200 && total <= 300) {
			desconto = (total * 20) / 100;
		} else if (total > 300 && total <= 400) {
			desconto = (total * 25) / 100;
		} else if (total > 400)
			desconto = (total * 30) / 100;

		return total - desconto;
	}

}
